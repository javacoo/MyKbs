# MyKbs

#### 介绍
丑牛迷你知识库是基于Java Swing开发的个人知识库系统，系统可在局域网内搜,共享积累的知识，可监听本地目录文件并建立全文索引，方便查找，可在局域网内多人聊天，视频，文件传输等<br/>
[MyKbs Maven版](https://gitee.com/javacoo/MyKbs-maven)

#### 软件架构
软件架构说明
#### 安装教程

#### 使用说明

1.我的丑牛：系统参数设置及插件信息

![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230240_c7bfa04f_121703.png "屏幕截图.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230315_08dc1d1f_121703.png "屏幕截图.png")<br/>
2.知识搜索：知识搜索分本地搜索和局域网搜索<br/>
本地搜索：勾选“按分类搜索”时，只搜索该分类下所有知识点，包含此关键字的信息靠前。
未勾选时会搜索所有知识分类下搜索包含此关键字的知识点。<br/>
勾选“按分类搜索”时。<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/232013_d50ed9fc_121703.png "屏幕截图.png")<br/>
未勾选“按分类搜索”时<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/232045_2c6f12c5_121703.png "屏幕截图.png")<br/>
局域网搜索：只要本地局域网内有其他用户使用了本系统，且分享了自己的知识积累，就可以搜索，查看或者下载<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/232109_76be4835_121703.png "屏幕截图.png")<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230345_4e5a1451_121703.png "屏幕截图.png")<br/>
3.  本地磁盘：对本地磁盘文件统一管理 <br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230405_ecf1890d_121703.png "屏幕截图.png")<br/>
4.知识管理：对知识程统一管理
系统默认自带一些常用知识分类 ，用户也可以自己添知识分类。
分类设置中“所属属性分组”是在参数管理中设置的。
选中一个知识分类即可添加知识点，知识点按内容形式分文文本内容和文件2中形式，可分别添加<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230436_edee33ab_121703.png "屏幕截图.png")<br/>
5.配置管理<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230500_701a9655_121703.png "屏幕截图.png")<br/>
6.在线网络配置：主要用于在局域网内分享自己的知识及在线聊天交流<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230525_9d947e8f_121703.png "屏幕截图.png")<br/>
7.索引词典配置：添加知识点时，需要为知识点建立索引，系统默认是根据系统分词器自带的词典，有一定的局限性，所以这里可以添加自己的词典，建立索引时就会结合自定义的词典进行分词处理，使搜索准确率更高<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230539_8497fedf_121703.png "屏幕截图.png")<br/>
8.知识索引管理：对已经建立的知识索引进行重建，主要是在添加了新的词语到索引词典里面时使用
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230552_4bb85160_121703.png "屏幕截图.png")<br/>
9.知识属性配置：主要用于用户自定义知识属性，系统默认只有少数必填的属性字段，用户可以按照需要自己扩展属性。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230607_e182e858_121703.png "屏幕截图.png")<br/>
10.局域网聊天室：一个简单的局域网聊天室，实现了文件传输，私聊，群聊，视频。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/230622_8ab02667_121703.png "屏幕截图.png")<br/>


#### 安装包下载地址
链接：https://pan.baidu.com/s/1ZRtN34NVk5mNwOhEqtzMRg 
提取码：uoi3 

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
